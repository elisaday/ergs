%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc 
%%% 连接管理器
%%% @end
%%%------------------------------------------------------------------------
-module(ergs_conn_mgr).
-behaviour(gen_server).

%% API
-export([
	 start_link/0,
	 add_conn/2,
	 remove_conn/1,
	 broadcast_msg/1,
	 show_conn_info/0,
	 send_msg/3
 	]).

%% gen_server callbacks
-export([
	 init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3
	]).

%% internal

%% internal state
-record(state, {tid}).

%% connection info
-record(conn_info, {id, pid}).

%%%================================================================
%%% API
%%%================================================================
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

add_conn(Pid, Id) ->
    gen_server:cast(?MODULE, {add_conn, Pid, Id}).

remove_conn(Pid) ->
    gen_server:cast(?MODULE, {remove_conn, Pid}).

broadcast_msg(Msg) ->
    {ok, Msg}.

show_conn_info() ->
    gen_server:cast(?MODULE, show_conn_info).

send_msg(ClientIdList, MsgLen, Msg) ->
    gen_server:cast(?MODULE, {send_msg, ClientIdList, MsgLen, Msg}).

%%%================================================================
%%% gen_server callbacks
%%%================================================================

init([]) ->
    Tid = ets:new(?MODULE, [private, {keypos, #conn_info.id}]),
    State = #state{tid = Tid},
    {ok, State}.

handle_call(_Msg, _From, State) ->
    {reply, ok, State}.

handle_cast({send_msg, ClientIdList, MsgLen, Msg}, #state{tid = Tid} = State) ->
    send_msg_to_client(Tid, ClientIdList, MsgLen, Msg),
    {noreply, State};

handle_cast(show_conn_info, #state{tid = Tid} = State) ->
    ConnList = ets:match(Tid, '$1'),
    show_conn_list(ConnList),
    {noreply, State};

handle_cast({add_conn, Pid, Id}, #state{tid = Tid} = State) ->
    error_logger:info_msg("add conn to conn_mgr[Pid: ~p Id: ~p]~n", [Pid, Id]),
    ets:insert(Tid, #conn_info{id = Id, pid = Pid}),
    {noreply, State};

handle_cast({remove_conn, Id}, #state{tid = Tid} = State) ->
    error_logger:info_msg("remove conn from conn_mgr[Id: ~p]~n", [Id]),
    ets:delete(Tid, Id),
    {noreply, State};

handle_cast(_Cast, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVersion, State, _Extra) ->
    {ok, State}.

%%%================================================================
%%% internal
%%%================================================================
show_conn_list([]) ->
    ok;

show_conn_list([Item|ConnList]) ->
    io:format("~p~n", [Item]),
    show_conn_list(ConnList).

send_msg_to_client(_Tid, [], _MsgLen, _Msg) ->
    ok;

send_msg_to_client(Tid, [Id|ClientIdList], MsgLen, Msg) ->
    case ets:lookup(Tid, Id) of
        [#conn_info{pid = Pid}] ->
	    gen_fsm:send_event(Pid, {send_msg, MsgLen, Msg});
	_ ->
	    ignore
    end,
    send_msg_to_client(Tid, ClientIdList, MsgLen, Msg).
