%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 网关服务器顶层监视进程
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_sup).
-behaviour(supervisor).

-export([
	 start_link/0,
	 init/1
	]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    RestartStrategy = one_for_one,
    AllowedRestarts = 1,
    MaxSeconds = 30,

    SupFlags = {RestartStrategy, AllowedRestarts, MaxSeconds},
    Restart = permanent,
    Shutdown = 2000,
    
    Listener = {ergs_listener, {ergs_listener, start_link, []}, Restart, Shutdown, worker, [ergs_listener]},
    ConnSup = {ergs_conn_sup, {ergs_conn_sup, start_link, []}, Restart, infinity, supervisor, [ergs_conn_sup]},
    ConnMgr = {ergs_conn_mgr, {ergs_conn_mgr, start_link, []}, Restart, Shutdown, worker, [ergs_conn_mgr]},
    SrvConnSup = {ergs_srv_conn_sup, {ergs_srv_conn_sup, start_link, []}, Restart, Shutdown, supervisor, [ergs_srv_conn_sup]},
    SrvMgr = {ergs_srv_mgr, {ergs_srv_mgr, start_link, []}, Restart, Shutdown, worker, [ergs_srv_mgr]},

    Children = [Listener, ConnSup, ConnMgr, SrvConnSup, SrvMgr],
    
    {ok, {SupFlags, Children}}.




