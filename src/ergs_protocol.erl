%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @copyright zeb
%%% @doc
%%% 协议处理工具函数
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_protocol).

-export([
	 check_msg/2,
	 make_msg/3,
	 make_msg/4,
	 make_msg/5
	]).

check_msg(client_to_ergs, Msg) ->
    case Msg of
	<<MsgBodyLen:16/integer, MsgBody:MsgBodyLen/binary, Remain/binary>> ->
	    {ok, MsgBodyLen, MsgBody, Remain};
	Other ->
	    {incomplete, Other}
    end;

check_msg(srv_to_ergs, Msg) ->
    case Msg of
	<<ClientNum:16/integer, PackedClientIdList:ClientNum/binary-unit:32,
	 MsgBodyLen:16/integer, MsgBody:MsgBodyLen/binary, Remain/binary>> ->
	    {ok, unpack_client_id_list(PackedClientIdList), MsgBodyLen, MsgBody, Remain};
	Other ->
	    {incomplete, Other}
    end.

make_msg(ergs_to_srv, ClientId, MsgLen, Msg) ->
    <<ClientId:32/integer, MsgLen:16/integer, Msg/binary>>.

make_msg(srv_to_ergs, ClientNum, ClientIdList, MsgLen, Msg) ->
    PackedClientIdList = pack_client_id_list(<<"">>, ClientIdList),
    <<ClientNum:16/integer, PackedClientIdList/binary, MsgLen:16/integer, Msg/binary>>.

make_msg(ergs_to_client, MsgLen, Msg) ->
    <<MsgLen:16/integer, Msg/binary>>.

pack_client_id_list(Packed, []) ->
    Packed;

pack_client_id_list(Packed, [ClientId|Tail]) ->
    NewPacked = <<Packed/binary, ClientId:32/integer>>,
    pack_client_id_list(NewPacked, Tail).

unpack_client_id_list(<<Id:32/integer>>) ->
    [Id];

unpack_client_id_list(<<Id:32/integer, PackedClientIdList/binary>>) ->
    [Id|unpack_client_id_list(PackedClientIdList)].





    
