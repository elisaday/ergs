%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 已认证状态
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_state_authenticated).

-include("ergs_conn.hrl").

-export([event/2]).

event(Data, #conn_state{id = ClientId, srv_id = SrvId, data = Remain} = State) ->
    NewData = <<Remain/binary, Data/binary>>,
    case ergs_protocol:check_msg(client_to_ergs, NewData) of
	{ok, MsgLen, Msg, RemainData} ->
	    ergs_srv_mgr:send_msg(SrvId, ClientId, MsgLen, Msg),
	    NewState = State#conn_state{data = RemainData},
	    {noreply, ergs_state_authenticated, NewState};	
	{incomplete, Other} ->
	    NewState = State#conn_state{data = Other},
	    {noreply, ergs_state_authenticated, NewState}
    end.
