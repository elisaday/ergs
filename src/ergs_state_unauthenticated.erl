%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc 
%%% 未认证状态
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_state_unauthenticated).

-include("ergs_protocol.hrl").
-include("ergs_conn.hrl").

-export([event/2]).

event(tcp_closed, _State) ->
    ok;

event(<<?ERGSP_AUTH:8/integer, 
	SrvId:16/integer, 
	AccLen:8/integer, Account:AccLen/binary, 
	PassLen:8/integer, Passwd:PassLen/binary>>, #conn_state{id = Id} = State) ->

    AccSrv = ergs_util:get_app_env(acc_srv), 
    Result = rpc:call(AccSrv, eras_srv, authenticate, [binary_to_list(Account), binary_to_list(Passwd)], 10000),
    error_logger:info_msg("auth result ~p~n", [Result]),
    case Result of
	ok ->
	    NewState = State#conn_state{srv_id = SrvId},
	    ergs_conn_mgr:add_conn(self(), Id),
	    {reply, <<?ERGSP_AUTH:8/integer, ?ERGSP_AUTH_SUCC:8/integer>>, ergs_state_authenticated, NewState};
	failed ->
	    {stop, auth_failed, <<?ERGSP_AUTH:8/integer, ?ERGSP_AUTH_FAILED:8/integer>>, State};
	{badrpc, timeout} ->
	    {stop, timeout, <<?ERGSP_AUTH:8/integer, ?ERGSP_AUTH_TIMEOUT:8/integer>>, State};
	{badrpc, Reason} ->
	    {stop, Reason, State};
	Error ->
	    error_logger:error_msg(Error),
	    {stop, internal_error, State}
    end;
	     
event(_Data, State) ->
    io:format("~p~n", [_Data]),
    {stop, normal, State}.

