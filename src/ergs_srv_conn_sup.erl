%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 服务器连接监控进程，负责创建服务器连接
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_srv_conn_sup).
-behaviour(supervisor).

-export([
	 start_conn/3,
	 start_link/0,
	 init/1
	]).

start_link() ->    
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    RestartStrategy = simple_one_for_one,
    AllowedRestarts = 1,
    MaxSeconds = 30,
    
    SupFlags = {RestartStrategy, AllowedRestarts, MaxSeconds},
    Restart = temporary,
    Shutdown = 6000,
    
    ConnSup = {ergs_srv_conn, {ergs_srv_conn, start_link, []}, Restart, Shutdown, worker, [ergs_srv_conn]},
    
    Children = [ConnSup],

    {ok, {SupFlags, Children}}.

start_conn(Id, Ip, Port) ->
    supervisor:start_child(?MODULE, [Id, Ip, Port]).
    
