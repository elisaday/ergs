%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc 
%%% 连接监控进程，负责创建连接进程
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_conn_sup).
-behaviour(supervisor).

-export([
	 start_conn/2,
	 start_link/0,
	 init/1
	]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    RestartStrategy = simple_one_for_one,
    AllowedRestarts = 1,
    MaxSeconds = 30,

    SupFlags = {RestartStrategy, AllowedRestarts, MaxSeconds},
    Restart = temporary,
    Shutdown = 6000,

    ConnSup = {ergs_conn, {ergs_conn, start_link, []}, Restart, Shutdown, worker, [ergs_conn]},

    Children = [ConnSup],
    
    {ok, {SupFlags, Children}}.

start_conn(ClientSocket, Id) ->
    supervisor:start_child(?MODULE, [ClientSocket, Id]).

