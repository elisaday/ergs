%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 工具函数
%%% @end
%%%------------------------------------------------------------------------

-module(ergs_util).

-export([get_app_env/1]).

get_app_env(Option) ->
    case application:get_env(ergs, Option) of
	{ok, Value} ->
	    Value;
	_ ->
	    case init:get_argument(Option) of
		[[Value|_]] ->
		    Value;
		error ->
		    undefined
	    end
    end.
