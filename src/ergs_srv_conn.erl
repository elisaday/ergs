%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 服务器连接
%%% @end
%%%------------------------------------------------------------------------
-module(ergs_srv_conn).
-behaviour(gen_server).

%% API
-export([
	 start_link/3,
	 send_msg/2
	]).

%% gen_server callbacks
-export([
	 init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3
	]).

%% internal

%% internal state
-record(state, {socket, id, data}).

%%%================================================================
%%% API
%%%================================================================
start_link(Id, Ip, Port) ->
    gen_server:start_link(?MODULE, [Id, Ip, Port], []).

send_msg(Pid, Msg) ->
    gen_server:cast(Pid, {send_msg, Msg}).

%%%================================================================
%%% gen_server callbacks
%%%================================================================
init([Id, Ip, Port]) ->
    case gen_tcp:connect(Ip, Port, [binary, {packet, 0}], 5000) of
	{ok, Socket} ->
	    State = #state{socket = Socket, id = Id, data = <<"">>},
	    {ok, State};
	Error ->
	    {stop, Error}
    end.

handle_call(_Msg, _From, State) ->
    {reply, ok, State}.

handle_cast({send_msg, Msg}, #state{socket = Socket} = State) ->
    gen_tcp:send(Socket, Msg),
    {noreply, State};

handle_cast(_Cast, State) ->
    {noreply, State}.

handle_info({tcp_closed, _ClientSocket}, State) ->
    {stop, normal, State};

handle_info({tcp, _ClientSocket, Data}, #state{data = Remain} = State) ->
    NewData = <<Remain/binary, Data/binary>>,
    case ergs_protocol:check_msg(srv_to_ergs, NewData) of
	{ok, ClientIdList, MsgLen, Msg, RemainData} ->	
	    ergs_conn_mgr:send_msg(ClientIdList, MsgLen, Msg),
	    NewState = State#state{data = RemainData},
	    {noreply, NewState};
	{incomplete, Other} ->
	    NewState = State#state{data = Other},
	    {noreply, NewState}
    end;
    
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #state{id = Id} = _State) ->
    error_logger:info_msg("srv conn disconnected[Id: ~p]~n", [Id]),
    ergs_srv_mgr:update_srv_status(Id, self(), disconnected),
    ok.

code_change(_OldVersion, State, _Extra) ->
    {ok, State}.

%%%================================================================
%%% internal
%%%================================================================
