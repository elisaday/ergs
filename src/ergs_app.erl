%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc 
%%% 网关应用程序
%%% @end
%%%------------------------------------------------------------------------
-module(ergs_app).
-behaviour(application).

-export([
	 start/0,
	 start/2,
	 stop/1
	]).

start() ->
    application:start(ergs).

start(_StartType, _StartArgs) ->
    {ok, Pid} = ergs_sup:start_link(),

    ServerList = ergs_util:get_app_env(srv_list),
    conn_srv(ServerList),
    ergs_srv_mgr:connect_all(),
    {ok, Pid}.

conn_srv([]) ->
    ok;

conn_srv([{Id, Ip, Port}|ServerList]) ->
    ergs_srv_mgr:add_srv(Id, Ip, Port),
    conn_srv(ServerList).

stop(_State) ->
    ok.

