%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 连接进程，负责处理连接各种状态下的数据分发
%%% @end
%%%------------------------------------------------------------------------
-module(ergs_conn).
-behaviour(gen_fsm).

-include("ergs_conn.hrl").

%% API

%% gen_server callbacks
-export([
	 start_link/2,
	 init/1,
	 handle_event/3,
	 handle_info/3,
	 handle_sync_event/4,
	 terminate/3,
	 code_change/4
	]).

%% internal

%% state
-export([
	 ergs_state_unauthenticated/2,
	 ergs_state_authenticated/2
	]).

%%%================================================================
%%% API
%%%================================================================

%%%================================================================
%%% gen_fsm callbacks
%%%================================================================
start_link(ClientSocket, Id) ->
    gen_fsm:start_link(?MODULE, [ClientSocket, Id], []).

init([ClientSocket, Id]) ->
    error_logger:info_report([{new_connection, {socket, ClientSocket}}]),

    State = #conn_state{id = Id, socket = ClientSocket},
    {ok, ergs_state_unauthenticated, State}.

handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

handle_info({tcp_closed, _ClientSocket}, StateName, State) ->
    apply(StateName, event, [tcp_closed, State]),
    {stop, normal, State}; 

handle_info({tcp, ClientSocket, Data}, StateName, #conn_state{socket = ClientSocket} = State) ->
    inet:setopts(ClientSocket, [{active, once}]),
    case apply(StateName, event, [Data, State]) of
        {reply, Reply, NextState, NewState} ->
            gen_tcp:send(ClientSocket, Reply),
            {next_state, NextState, NewState};
        {noreply, NextState, NewState} ->
            {next_state, NextState, NewState};
	{stop, Reason, Reply, NewState} ->
            gen_tcp:send(ClientSocket, Reply),
	    {stop, Reason, Reply, NewState};
	{stop, Reason, NewState} ->
	    {stop, Reason, NewState};
        Error ->
            error_logger:error_report([Error]),
            {stop, internal_error, State}
    end;

handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.
 
handle_sync_event(_Event, _From, StateName, State) ->
    {reply, test, StateName, State}.

code_change(_OldVsn, _StateName, _State, _Extra) ->
    ok.

terminate(_Reason, _StateName, #conn_state{id = Id} = _State) ->
    ergs_conn_mgr:remove_conn(Id),
    ok.

%%%================================================================
%%% internal
%%%================================================================
ergs_state_unauthenticated(_Event, State) ->
    {stop, internal_error, State}.

ergs_state_authenticated({send_msg, MsgLen, Msg}, #conn_state{socket = ClientSocket} = State) ->
    PackedMsg = ergs_protocol:make_msg(ergs_to_client, MsgLen, Msg),
    gen_tcp:send(ClientSocket, PackedMsg),
    {next_state, ergs_state_authenticated, State}.

