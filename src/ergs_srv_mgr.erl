%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @doc
%%% 服务器管理器
%%% @end
%%%------------------------------------------------------------------------
-module(ergs_srv_mgr).
-behaviour(gen_server).

%% API
-export([
	 start_link/0,
	 add_srv/3,
	 connect_all/0,
	 show_srv_info/0,
	 update_srv_status/3,
	 send_msg/4
	]).

%% gen_server callbacks
-export([
	 init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3
	]).

%% internal
-export([
	 connect/2,
	 set_srv_status/4
	]).

%% internal state
-record(state, {tid}).

%% server info
-record(srv_info, {id, ip, port, pid, status}).

%%%================================================================
%%% API
%%%================================================================
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

add_srv(Id, Ip, Port) ->
    gen_server:call(?MODULE, {add_srv, Id, Ip, Port}).

connect_all() ->
    gen_server:cast(?MODULE, connect_all).

show_srv_info() ->
    gen_server:cast(?MODULE, show_srv_info).

update_srv_status(Id, Pid, Status) ->
    gen_server:cast(?MODULE, {update_srv_status, Id, Pid, Status}).

send_msg(SrvId, ClientId, MsgLen, Msg) ->
    gen_server:cast(?MODULE, {send_msg, SrvId, ClientId, MsgLen, Msg}).

%%%================================================================
%%% gen_server callbacks
%%%================================================================
init([]) ->
    Tid = ets:new(?MODULE, [private, {keypos, #srv_info.id}]),
    State = #state{tid = Tid},
    {ok, State}.

handle_call({add_srv, Id, Ip, Port}, _From, #state{tid = Tid} = State) ->
    case ets:lookup(Tid, Id) of
	[_] ->
	    {reply, exist, State};
	[] ->
	    ets:insert(Tid, #srv_info{id = Id, ip = Ip, port = Port, pid = pull, status = disconnected}),
	    {reply, ok, State}
    end;

handle_call(_Msg, _From, State) ->
    {reply, ok, State}.

handle_cast({send_msg, SrvId, ClientId, MsgLen, Msg}, #state{tid = Tid} = State) ->
    NewMsg = ergs_protocol:make_msg(ergs_to_srv, ClientId, MsgLen, Msg),
    case ets:lookup(Tid, SrvId) of
	[#srv_info{pid = Pid}] ->
	    ergs_srv_conn:send_msg(Pid, NewMsg),
	    {noreply, State};
	[] ->
	    {noreply, State}
    end;
    
handle_cast(connect_all, #state{tid = Tid} = State) ->
    SrvList = ets:match(Tid, '$1'),
    connect(Tid, SrvList),
    {noreply, State};

handle_cast(show_srv_info, #state{tid = Tid} = State) ->
    ServerList = ets:match(Tid, '$1'),
    show_srv_list(ServerList),
    {noreply, State};

handle_cast({update_srv_status, Id, Pid, Status}, #state{tid = Tid} = State) ->
    set_srv_status(Tid, Id, Pid, Status),
    {noreply, State};
    
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVersion, State, _Extra) ->
    {ok, State}.

%%%================================================================
%%% Internal
%%%================================================================
set_srv_status(Tid, Id, Pid, Status) ->
    ets:update_element(Tid, Id, [{#srv_info.pid, Pid}, {#srv_info.status, Status}]).

connect(_Tid, []) ->
    done;

connect(Tid, [[#srv_info{id = Id, ip = Ip, port = Port, status = Status}]|SrvList]) ->
    case Status of
	disconnected ->
	    error_logger:info_report({"connect to server", {id, Id}, {ip, Ip}, {port, Port}}),
	    case ergs_srv_conn_sup:start_conn(Id, Ip, Port) of
		{ok, Pid} ->
		    set_srv_status(Tid, Id, Pid, connected),
		    error_logger:info_report("connect ok");
		Error  ->
		    error_logger:error_report(["connect failed", Error])
	    end;
	connected ->
	    ignore
    end,
    
    connect(Tid, SrvList).

show_srv_list([]) ->
    ok;

show_srv_list([Item|SrvList]) ->
    io:format("~p~n", [Item]),
    show_srv_list(SrvList).
		       
